const effectsConfig = {
  times: ['once', 'many'],
  reach: ['one', 'many', 'all enemies', 'all alies', 'all'],
  amount: ['fixed', 'max-min', 'percent']
}

const effectsTypes = {
  damage: ['normal', 'direct', 'shield'],
  shield: ['shield'],
  heal: ['heal'],
  speed: ['speed']
}