const events = require('events')
const { Entity } = require('../db/models')
const { buildAttack, resolveAtack } = require('./effects_functions')

const effectsEmitter = new events.EventEmitter()

effectsEmitter.on('DAMAGE_NORMAL', async (io,target, attack) => {
  
  const msg = `${target.name}: Prev HP: ${target.hp}, Prev SH: ${target.shield}, Amount: ${attack.amount}`
  target.shield -= attack.amount
  if (target.shield < 0) {
    target.hp += target.shield
    target.shield = 0
  }


  const doc = await Entity.findByIdAndUpdate(target._id, {hp: target.hp, shield: target.shield}, {new: true})
  console.log('DN: ', doc)
  io.emit('dataUpdate', {doc, msg})
})

effectsEmitter.on('DAMAGE_DIRECT', async (io,target, attack) => {
  const msg = `${target.name}: Prev HP: ${target.hp}, Prev SH: ${target.shield}, Amount: ${attack.amount}`
  
  target.hp -= attack.amount

    
  const doc = await Entity.findByIdAndUpdate(target._id, {hp: target.hp}, {new: true})
  console.log('DD: ', doc)
  io.emit('dataUpdate', doc)
})

effectsEmitter.on('DAMAGE_SHIELD', async (io,target, attack) => {
   
  target.shield -= attack.amount
  if (target.shield < 0) {
    target.shield = 0
  }
    
  const doc = await Entity.findByIdAndUpdate(target._id, {shield: target.shield}, {new: true})
  console.log('DS: ', doc)
  io.emit('dataUpdate', doc)
})

effectsEmitter.on('SHIELD', async (io,target, attack) => {
   
  target.shield += attack.amount
  
  const doc = await Entity.findByIdAndUpdate(target._id, {shield: target.shield}, {new: true})
  console.log('SHIELD: ', doc)
  io.emit('dataUpdate', doc)
})

effectsEmitter.on('HEAL', async (io,target, attack) => {
 
  const msg = `${target.name}: Prev HP: ${target.hp}, Prev SH: ${target.shield}, Amount: ${attack.amount}`
  target.hp += attack.amount
  if (target.hp > target.maxHp) {
    target.hp = target.maxHp
  }

  const doc = await Entity.findByIdAndUpdate(target._id, {hp: target.hp}, {new: true})
  console.log('HEAL: ', doc)
  io.emit('dataUpdate', {doc, msg})
})

effectsEmitter.on('SPEED', async (io,target, attack) => {
  
})

module.exports = effectsEmitter