const events = require('events')

const effectsEmitter = require('../effects/effects_events')
const { buildAttack, resolveAtack } = require('../effects/effects_functions')

const gameFlowEmitter = new events.EventEmitter()

gameFlowEmitter.on('ATTACK', async (io,attacker, attack, targets) => {

  let targetsNames = ''
  targets.forEach(target => {
    targetsNames += `${target.name} `
  })
  let msg = `${attacker.name} did ${attack.name} to ${targetsNames}`
  io.emit('displayMsg', msg)
  
  attack.effects.forEach(async (effect, index) => {
    
    // Resolve attack from entity's side
    const attack = await buildAttack(attacker, effect)
    

    setTimeout(async() => { // to be executed in the callback phase

      // For each target of a sinlge effect of the attack
      for (const target of targets[index]) {
      
        // resolve the final amount to target
        const thisAttack = await resolveAtack(target, attack)

        // resolve the attack according to the type of effect
        effectsEmitter.emit(effect.name, io, target, thisAttack)
  
        effect.times --

        // if (effect.times > 0) {
        //   let timer = setInterval(() => {
        //     if (effect.times <= 0) {
        //       clearInterval(timer)
        //     } else {
        //       effectsEmitter.emit(effect.name, attacker, req.body.targets[index], effect)
        //       effect.times --
        //       console.log('rests: ', effect.times)
        //     }
        //   }, effect.interval)
        // }
      }
  
    },0) //effect.timeout)
  })
})

module.exports = gameFlowEmitter