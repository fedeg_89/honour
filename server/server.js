const express = require('express')
const app = express()
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const path = require('path')
const cors = require('cors')
const mongoose = require('mongoose')

app.use(express.static(path.join(__dirname, '../public')))
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(cors())

const effectsEmitter = require('./effects/effects_events')
const gameFlowEmitter = require('./gameFlow/gameFlow_events')


const { PlayerClass, Attack, Effect, Entity, EffectType, StatsTypes } = require("./db/models")

const monster_id = '5e5c17f58d8fcb1dd87eb7fa'
const player1_id = '5e5c1ee58d8fcb1dd87eb7fb'


io.on('connection', async function(socket){
  console.log('A warrior entered the battlefield!')
  const attacks = await Attack.find()
  const effects = await Effect.find()
  const monsters = await Entity.find({type: 2})
  const players = await Entity.find({type: 1})
  socket.emit('initialData',{ attacks, effects, monsters, players})


  socket.on('attack', async body => {
    // Get data
    const attacker = await Entity.findById(player1_id).populate('side')

    const attack = await Attack.findById(body.attack_id)
    await EffectType.populate(attack.effects[0], {path: 'type'})
    await StatsTypes.populate(attack.effects[0], {path: 'stat'})

    const targetsId = body.targets_ids
    const targets = []
    for (const targetArr of targetsId) {
      await Entity.find({
        '_id': { $in: targetArr}
      })
      .populate('side')
      .then(target => {
        targets.push(target)
      })
    }

    // The attack
    gameFlowEmitter.emit('ATTACK',io, attacker, attack, targets )
  })





})

// Attack.findById(1)
// .then(attack => {
//   EffectType.populate(attack.effects[0], {path: 'type'}).then(data => {

//     console.log(data)
//   })
//   console.log(attack)
// })


app.post('/at', async (req, res) => {

  // Get data
  const attacker = await Entity.findById(player1_id).populate('side')

  const attack = await Attack.findById(req.body.attack_id)
  await EffectType.populate(attack.effects[0], {path: 'type'})
  await StatsTypes.populate(attack.effects[0], {path: 'stat'})

   
  // const targets = await Entity.find({
  //   '_id': { $in: [
  //     mongoose.Types.ObjectId('5e5c17f58d8fcb1dd87eb7fa'),
  //     mongoose.Types.ObjectId('5e5c1ee58d8fcb1dd87eb7fb'), 
  //   ]}
  // })

  const targetsId = req.body.targets_ids
  // const targetsId = [
  //   [monster_id, player1_id],
  //   [player1_id]
  // ]
  const targets = []
  
  for (const targetArr of targetsId) {
    await Entity.find({
      '_id': { $in: targetArr}
    })
    .populate('side')
    .then(target => {
      targets.push(target)
    })
  }

  // console.log('targets: ', targets)

  // The attack
  gameFlowEmitter.emit('ATTACK', attacker, attack, targets )

  res.send({msg: 'ok'})
})


// Main endpoints
app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, '../public/index.html'))
})

app.get('*', (req, res) => {
	res.sendStatus(404)
})

const port = process.env.PORT || 3000
http.listen(port, () => console.log(`The battle starts on port ${port}`))