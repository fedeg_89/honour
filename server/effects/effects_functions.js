const { Side } = require('../db/models')

module.exports = {
  async buildAttack(attacker, effect) {
    // console.log(attacker)
    // Initial value
    let attack = {
      amount: attacker.stats[effect.stat.name] * effect.multiplier,
      stat: effect.stat._id,
      type: effect.type._id
    } 

    console.log('at 1: ', attack.amount)
    // Entity modifiers
    attack = throughModifiers(attacker.attackModifiers, attack)
    
    console.log('at 2: ', attack.amount)

    // Side modifiers
    const side = await Side.findById(attacker.side)
    attack = throughModifiers(side.attackModifiers, attack)
    
    console.log('at 3: ', attack.amount)

    // enviroment state
    return attack
  },
  async resolveAtack(defender, attack) {
    // Side modifiers
    const side = await Side.findById(defender.side)
    attack = throughModifiers(side.defenseModifiers, attack)

    console.log('def 1: ', attack.amount)

    // Entity modifiers
    attack = throughModifiers(defender.defenseModifiers, attack)
    
    console.log('def 2: ', attack.amount)

    return attack
  }

 
}

function throughModifiers(modifiers, attack) {
  // console.log('modif: ', modifiers)
  // console.log('attack: ', attack)
  modifiers.forEach(modifier => {
    if (modifier.stat == attack.stat && modifier.type == attack.type) {
      attack.amount += modifier.amount
    }
  })

  return attack
}