const attack1 = {
  effects: [
    {
      type: 'damage',
      times: 5,
      //reach: 'one' // viene en target []
      amount: 50
    },
    {
      type: 'heal',
      times: 1,
      //reach: 'one' // viene en target []
      amount: 99
    }
  ],
  speed: 5000,
  type: 'eventName'

}

module.exports = attack1