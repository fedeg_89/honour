const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)
mongoose.set('useUnifiedTopology', true)
mongoose.set('useFindAndModify', false)
// mongoose.set('debug', true)
mongoose.Promise = global.Promise

// const urlAtlas = "mongodb+srv://fede:DoMiSolSib@honour-on9ci.gcp.mongodb.net/honour?retryWrites=true&w=majority"
mongoose.connect("mongodb://localhost:27017/honour", { useNewUrlParser: true })
// mongoose.connect("mongodb://localhost:9999/honour", { useNewUrlParser: true })
// mongoose.connect(urlAtlas, { useNewUrlParser: true })

const Schema = mongoose.Schema

// ********* Models ******** //
/////////// MODIFIERS ///////////
const modifierSchema = new Schema({
	stat : {
		type: Number,
		required: true,
		ref: 'statType'
	},
	type: {
		type: Number,
		required: true,
		ref: 'effectType'
	},
	amount: {
		type: Number,
		required: true
	}
})

const Modifier = mongoose.model("modifier", modifierSchema)

exports.Modifier = Modifier

/////////// CLASSES ///////////
const classSchema = new Schema({
  _id: {
    type: Number,
    required: true
  },
  name: {
		type: String,
		required: true,
    trim: true,
    unique: true		
	},
	maxHp : {
		type: Number,
		required: true,
	},
	shield : {
		type: Number,
		required: true,
	},
	damage : {
		type: Number,
		required: true,
	},
	canAttack : {
		type: Boolean,
    default: true
	},
	modifiers: [{
    type: modifierSchema,
  }],
	possibleStates: [{
    type: Number,
    ref: "entityState"
  }],
	attacks: [{
    type: Number,
    ref: "attack"
  }],
})

classSchema.methods.addUrl = function(){
	// const url = slugify(this.name, slugifyOptions);

	// this.set({ url: url });
	// this.save(function (err, game) {
	// if (err) return err;
	// });
}

const PlayerClass = mongoose.model("class", classSchema)

exports.PlayerClass = PlayerClass

/////////// EFFECTS  ///////////

const effectSchema = new Schema ({
  _id: {
    type: Number,
    required: true
  },
	name : {
		type: String,
		required: true,
    trim: true,
    unique: true		
  },
  times : {
		type: Number,
		required: true,
	},
  interval : {
		type: Number,
		required: true,
	},
  timeout : {
		type: Number,
		required: true,
	},
  type : {
    type: Number,
		// type: Schema.Types.ObjectId,
    ref: "effectType"
	},
  stat : {
    type: Number,
		// type: Schema.Types.ObjectId,
    ref: "statType"
	},
  multiplier : {
		type: Number,
		// required: true,
	},
	amount: {
		type: Number
	}
})

const Effect = mongoose.model("effect", effectSchema)
exports.Effect = Effect

/////////// EFFECT TYPES  ///////////

const effectTypesSchema = new Schema ({
  _id: {
    type: Number,
    required: true
  },
	name : {
		type: String,
		required: true,
    trim: true,
    unique: true		
  }
}, { collection: 'effectTypes' })

const EffectType = mongoose.model("effectType", effectTypesSchema)
exports.EffectType = EffectType


/////////// ATTACKS ///////////
const attackSchema = new Schema({
  _id: {
    type: Number,
    required: true
  },
	name : {
		type: String,
		required: true,
    trim: true,
    unique: true		
	},
	recharges : {
		type: Number,
		required: true,
	},
	effects: [{
    type: effectSchema
    // ref: "effect"
  }],

})

const Attack = mongoose.model("attack", attackSchema)
exports.Attack = Attack

/////////// ENTITIES ///////////
const entitySchema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true,
	},
  class: {
    type: Number,
    required: true,
    ref: 'class'
  },
	type: {
		type: Number,
		ref: 'entityType',
		default: 1
	},
	zone: {
		type: Number,
		ref: 'zone',
		default: 1
	},
	side: {
		type: Number,
		ref: 'side',
		default: 1
	},
	maxHp : {
		type: Number,
		required: true,
	},
  hp: {
    type: Number,
		required: true,
  },
	shield : {
		type: Number,
		required: true,
	},
	stats : {
		physical: { type: Number },
    magical: { type: Number },
    elemental: { type: Number },
    spiritual: { type: Number }
	},
	canAttack : {
		type: Boolean,
    default: true
	},
	attackModifiers: [{
    type: modifierSchema,
  }],
	defenseModifiers: [{
    type: modifierSchema,
  }],
	possibleStates: [{
    type: Number,
    ref: "entityState"
  }],
	attacks: [{
    type: Number,
    ref: "attack"
  }],
})


const Entity = mongoose.model("entity", entitySchema)

exports.Entity = Entity

/////////// STATS ///////////
const statsSchema = new Schema({
	_id: {
    type: Number,
    required: true
  },
	name : {
		type: String,
		required: true,
    trim: true,
    unique: true		
  }
}, { collection: 'statsTypes' })

const StatsTypes = mongoose.model("statType", statsSchema)

exports.StatsTypes = StatsTypes

/////////// SIDES ///////////
const sideSchema = new Schema({
	_id: {
    type: Number,
    required: true
  },
	name : {
		type: String,
		required: true,
    trim: true,
    unique: true		
	},
	attackModifiers: [{
    type: modifierSchema,
  }],
	defenseModifiers: [{
    type: modifierSchema,
  }],
})

const Side = mongoose.model("side", sideSchema)

exports.Side = Side


