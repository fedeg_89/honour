class Warrior {
  constructor(id) {
    this.id = id

    this.hp = 200
    this.maxHp = 200
    this.shield = 100
    this.canPlay = true

    this.slash = {
      waitingTime: 5000,
      type: 'hp',
      effects: [
        {
          times: {
            times: 1,
            interval: 0,
            time: 0
          },
          reach: {
            alies: 0,
            enemies: 1
          },
          amount: -50,
          type: 1 // physical
        }
      ]
    }
  }

  toggleCanPlay() {
    this.canPlay = !this.canPlay
  }
}

module.exports = Warrior